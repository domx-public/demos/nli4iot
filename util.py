import numpy as np
import pandas as pd
# util.py
import plotly.graph_objects as go


def plot_gas_consumption(gas_consumption):
    """
    Plots the gas consumption over time using Plotly.

    Parameters:
    gas_consumption (pd.Series): A pandas Series with timestamps as index and gas consumption values.
    """
    fig = go.Figure()

    fig.add_trace(go.Scatter(
        x=gas_consumption.index,
        y=gas_consumption.values,
        mode='lines',
        name='Gas Consumption'
    ))

    fig.update_layout(
        title='Gas Consumption Over Time',
        xaxis_title='Timestamp',
        yaxis_title='Gas Consumption'
    )

    fig.show()


def calc_gas_consumption(bml_series, max_capacity=24, dt=1, freq=False, closed='left', label='left'):
    '''
    Calculates the boiler gas consumption in kWh


    Parameters
    ----------

    bml_series: pandas.Series
        object with blr_mod_lvl measurements
        clean and resampled data should be given

    freq: {'H', 'D'}, default False
        calculate consuption per day ('D') or hour ('H')


    Returns
    -------

    numpy.float64
        the calculated gas consumption in kWh

    pandas.Series, if freq = 'D' or 'H'
        the consumption per day or hour
    '''

    # convert to kW
    bml_series = bml_series * max_capacity / 100

    # convert to kWh
    if freq is False:
        gas_con = np.trapz(bml_series.values) / (3600 / dt)
    else:
        gas_con = bml_series.groupby(pd.Grouper(freq=freq, closed=closed, label=label)).aggregate(np.trapz) / (
                3600 / dt)

    return gas_con
