# NLI4IoT

## Project Objectives
- **Natural Language Interfaces (NLIs)**:

  - Develop NLIs to enable conversational interaction with IoT databases.
  - Improve accessibility and ease of use for all users.
- **Enhanced Data Accessibility:**
  - Simplify the process of querying and retrieving IoT data.
  - Make data more accessible to users regardless of technical expertise.
- **Improved Interpretability:**

  - Address ambiguity in natural language queries.
  - Ensure accurate data interpretation and retrieval.
- **Privacy Protection:**

  - Implement robust privacy measures in data handling.
  - Ensure secure access to sensitive IoT data.

## Background
DomX offers a set of cost-effective and universal IoT controllers for upgrading legacy HVAC appliances (natural gas boilers, heat pumps, ACs) into smart flexible assets able to participate in energy markets. It builds on a set of open standards (Opentherm, ModBus) and widely applicable control modes (ON/OFF, power modulation, etc.) for enabling interoperability across energy vectors (natural gas, electricity), energy (forecasting, flexibility) and non-energy services (comfort, maintenance).
- DOMX smart heating controller for legacy HVAC equipment: Natural Gas boilers employing the dominant European Opentherm standard, Heat-pumps employing the widely applicable Modbus data communication protocol, older HVAC units (oil, gas), electric water and space heaters managed through ON/OFF control,  legacy split-unit ACs managed through the domX IR emulator Hardware solutions for interoperability. 
- DOMX Indoor Air Quality sensor: Advanced Air Quality Monitoring for Domestic and Commercial buildings, One high-end sensor per building - coupled with additional low-cost sensors, Accurate Temperature, Humidity, Barometric Pressure and VOC readings, Particulate Matter detection (PM2.5, PM10), CO2 levels, Internal logging of past measurements, edge computing through TinyML
- DOMX energy meter: Real-time smart meter, able to monitor up to three independent measurement channels. It reports power, voltage, current, overall energy, and power factor and supports contactor control.

The DOMX smartphone application enables end consumers to understand how energy is consumed and to achieve long-term consumer behavior change through nudging interventions. The edge devices connect over Wi-Fi to the Internet and the company’s cloud energy management platform, enabling adaptation to building characteristics (envelope, boiler), climate variations (indoor, outdoor) and user schedules-preferences.

![DOMX](image.png)


DOMX has developed a ML-based framework that produces the digital twin representation of natural gas boilers (as synthetic time-series), emulating their operation under different configurations.

## Usage
**Note**: You will need your own API key for a LLM to run the code!


- Clone the repository
- Create the Conda environment:
    ```
    conda env create -f environment.yml
    ```
- Activate the environment:
  ```
  conda activate your_env_name
  ```
- Run the jupyter notebook



The sample code is provided as a Jupyter notebook. The code loads a dataset (as described below)
containing data for a household. It preprocesses the data and creates 2 csv files, one containing measurements
for indoor-outdoor temperature sampled by minute and another containing the daily gas consumption of the household.

Relying on those 2 csv files basic pandas operations are performed to derive the answers to questions like the following, while the exact same questions are provided to a LLM using [PandasAI](https://pandas-ai.com/).

**Q1. "Can you show me the total gas consumption for each month as a bar chart?"**

A1. The LLM responded with the figure below: 

![Gas Consumption](q2.PNG)

**Q2. "Plot a line plot of the average indoor temperature per month"**

A2. The LLM responded with the figure below: 

![Indoor Temperature](q1.PNG)

**Q3. "Show me the top 5 days with the highest gas consumption"**

A3. The LLM responded with the table below: 

![Top 5](q3.png)



## Dataset

The provided dataset includes data collected from the DOMX smart HVAC controller from 5 pilot homes during the winter season of 2022 - 2023. The domx HVAC controller continuously collects indoor ambient data and data from the outdoor environment, as well as data produced by the heating system's activity, including indoor and outdoor temperatures, indoor temperature setpoint, heating outlet and return temperatures, setpoint and load level among other metrics.

A high-level description of the monitored variables included in the dataset is provided below:

- **time**: timestamp in YY-mm-DD HH:MM:SS format
- **blr_mod_lvl**: boiler load level expressed as a percentage of maximum boiler modulation capacity
- **water**: binary variable which indicates whether domestic hot water is requested
- **blr_t**: the current temperature of the water inside the boiler
- **t_ret**: the current temperature of the water entering the boiler from the radiators
- **t_out**: the current outdoor temperature
- **t_r**: the current indoor temperature
- **t_set**: boiler setpoint - the target water temperature the boiler is instructed to reach
- **t_r_set**: indoor setpoint - the target room temperature the heating system must reach
- **bypass**: binary variable which indicates whether the adaptive heating algorithm is active or not. In case this is 0, the legacy mode of the boiler is active which always sets the boiler setpoint to a fixed temperature, typically within the range of 65-80°C
- **otc_maxt**: optmized boiler setpoint which is instructed when adaptive heating is active
